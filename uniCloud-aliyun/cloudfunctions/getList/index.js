'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	const query = event.httpMethod ? event.body ? JSON.parse(event.body) : event.queryStringParameters : event;
	const { page = 1, pageSize = 10 } = query;
	const s = pageSize * (page - 1);
	const countRes = await db.collection('list').count();
	const res = await db.collection('list').skip(+s).limit(+pageSize).get();
	return {
		code: 200,
		count: countRes.total,
		datas: res.data,
		query: query
	}
};
