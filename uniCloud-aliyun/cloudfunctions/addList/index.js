'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event : ', event)
	let data = [];
	for(let i=1;i<=100;i++){
		data.push({
			title: `标题${i}`,
			sub: `副标题${i}`,
			image: "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-44851623-8c92-44c2-9205-8ff621f7e05b/ebdfa9ba-b925-43b2-b02a-613cd34648f7.jpg"
		})
	}
	console.log(data)
	const add = await db.collection('list').add(data);
	//返回数据给客户端
	return add
};
