/**
 * uniapp之请求封装（利用async+await和promise的思想）
 * options {Object} 参数
 * options.url {String} 请求地址
 * options.data {Object} 请求参数
 * options.method {String} 请求方法 默认GET
 * options.host {Boolean} 请求的地址标识 默认api
 * options.successCode {Number} 请求成功后的code 默认200
 * options.loginLoseCode {Number} token失效的code 默认loginLoseCode
 * options.type {String||Number} 提示窗口提示 0请求前后不提示  1请求前提示  2请求后提示 3请求前后提示
 * options.bMsg {String} 请求前提示
 * options.eMsg {String} 请求后提示
 * options.iconType {String} 请求成功后提示icon类型 默认success
 * options.cType {String} Content-Type的类型：一般有application/json、application/x-www-form-urlencoded等
 * options.mask {Boolean} 请求加载是否有遮罩 默认true
 * options.back {Boolean} 请求结束是否返回 默认false
 * options.backTime {Number} 请求结束返回等待的时间  默认500
 * options.failBack 错误数据是否返回 0不返回  1返回  默认0
 * options.notFailMsg {Boolean} 错误信息是否不提示 默认false
 * options.directBack 是否直接返回结果 默认false
 * options.notHideLoading 是否不关闭加载等待 默认false
 */
import config from '@/config.js';
const request = {
	async init(options) {
		// 解构赋值
		let {
			url,
			data,
			method = 'GET',
			host = 'api',
			successCode = 200,
			loginLoseCode = 30000,
			duration = 500,
			type = 0,
			bMsg = '',
			eMsg = '',
			iconType = 'success',
			cType = 'application/json', //application/x-www-form-urlencoded
			mask = true,
			back = false,
			backTime = 200,
			failBack = 0,
			notFailMsg = false,
			directBack = false,
			notHideLoading = false
		} = options;
		let header = {};
		// 请求地址
		host = config.host[host];
		// #ifdef H5
		// 生产环境时/api可去掉
		url = process.env.NODE_ENV == 'development' ? `/api${url}` : url;
		host = process.env.NODE_ENV == 'development' ? '' : host;
		// #endif
		// 根据业务需求添加header参数等...
		header['Content-Type'] = cType;
		// 请求前的加载
		if (type == 1 || type == 3) {
			uni.showLoading({
				title: bMsg,
				mask: mask
			});
		};
		// #ifdef VUE3
		try {
			var res = await uni.request({
				url: `${host}${url}`,
				data,
				method,
				header
			});
		} catch (e) {
			uni.showToast({
				title: '网络出小差了~',
				icon: "none"
			});
			return;
		}
		// #endif
		// #ifndef VUE3
		// 执行请求
		var [error, res] = await uni.request({
			url: `${host}${url}`,
			data,
			method,
			header
		});
		// 网络出错
		if (error) {
			uni.showToast({
				title: '网络出小差了~',
				icon: "none"
			});
			return;
		}
		// #endif
		// 请求成功处理逻辑
		if (!notHideLoading && (type == 1 || type == 3)) uni.hideLoading(); // 关闭请求加载
		const res_data = res.data;
		switch (+res_data.code) {
			case successCode: //请求成功
				if (type == 2 || type == 3) {
					if (back) {
						const backFn = () => {
							return new Promise((resolve, reject) => {
								setTimeout(async () => {
									// #ifdef VUE3
									try {
										var toastRes = await uni.showToast({
											title: eMsg ? eMsg : '请求成功',
											mask: true,
											icon: iconType,
											duration: duration
										});
									} catch (e) {}
									// #endif
									// #ifndef VUE3
									var [toastError, toastRes] = await uni.showToast({
										title: eMsg ? eMsg : '请求成功',
										mask: true,
										icon: iconType,
										duration: duration
									});
									// #endif
									if (toastRes && toastRes.errMsg == 'showToast:ok') {
										setTimeout(() => {
											resolve(true)
										}, duration + backTime);
									}
								}, 100)
							})
						}
						const backRes = await backFn();
						if (backRes) return res_data;
					} else {
						setTimeout(() => {
							uni.showToast({
								title: eMsg ? eMsg : '请求成功',
								mask: true,
								icon: iconType
							})
						}, 100)
						return res_data;
					}
				} else {
					return res_data;
				}
				break;
			case loginLoseCode: //登录过期
				// 处理登录失效的逻辑，比如，可以跳转到登录页
				uni.reLaunch({
					url:'/pages/login/login'
				})
				break;
			default:
				if (failBack == 1) Promise.reject(res_data);
				if (notFailMsg) return;
				const message = res_data.message || res_data.msg || '请求失败';
				if (message && message.length > 20) {
					uni.showModal({
						content: message,
						showCancel: false
					})
					return false;
				}
				setTimeout(() => {
					uni.showToast({
						icon: 'none',
						title: message
					})
				}, 100)
				break;
		}
	}
};
export default request.init;
