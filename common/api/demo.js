import Request from '@/common/js/request.js';
export function getList(data, type) {
	// return Request({
	// 	url: "/getTestList",
	// 	bMsg: "加载中",
	// 	data,
	// 	type: type
	// })
	const { page = 1, pageSize = 10 } = data;
	return new Promise((resolve, reject) => {
		uni.request({
			url: `https://44851623-8c92-44c2-9205-8ff621f7e05b.bspapp.com/getTestList`,
			data: {
				page: page,
				pageSize: pageSize
			},
			// method:'POST',
			success(res) {
				resolve(res.data);
			}
		})
	})
}
